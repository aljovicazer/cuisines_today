package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import static com.google.common.collect.ImmutableList.copyOf;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptySet;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private Map<Cuisine, Set<Customer>> cuisineToCustomers = new HashMap<>();
    private Map<Customer, Set<Cuisine>> customerToCuisines = new HashMap<>();

    @Override
    synchronized public void register(final Customer customer, final Cuisine cuisine) {
        cuisineToCustomers.computeIfAbsent(cuisine, ignore -> new HashSet<>()).add(customer);
        customerToCuisines.computeIfAbsent(customer, ignore -> new HashSet<>()).add(cuisine);
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
        Set<Customer> customers = cuisineToCustomers.getOrDefault(cuisine, emptySet());

        return copyOf(customers);
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        Set<Cuisine> cuisines = customerToCuisines.getOrDefault(customer, emptySet());

        return copyOf(cuisines);
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
        if (n < 1) {
            System.out.println(String.format("N=%s should be a positive integer", n));
            return emptyList();
        }

        return cuisineToCustomers.entrySet().stream()
                .sorted(compareByCustomerSize())
                .map(Entry::getKey)
                .limit(n)
                .collect(toList());
    }

    private Comparator<Entry<Cuisine, Set<Customer>>> compareByCustomerSize() {
        return comparingInt((Entry<Cuisine, Set<Customer>> e) -> e.getValue().size()).reversed();
    }
}
