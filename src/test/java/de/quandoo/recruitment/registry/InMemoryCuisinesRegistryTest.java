package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryCuisinesRegistryTest {

    private static final Cuisine FRENCH = new Cuisine("french");
    private static final Cuisine GERMAN = new Cuisine("german");
    private static final Cuisine ITALIAN = new Cuisine("italian");
    private static final Cuisine VIETNAMESE = new Cuisine("vietnamese");

    private static final Customer CUSTOMER1 = new Customer("1");
    private static final Customer CUSTOMER2 = new Customer("2");
    private static final Customer CUSTOMER3 = new Customer("3");
    private static final Customer CUSTOMER4 = new Customer("4");

    private CuisinesRegistry cuisinesRegistry;

    @Before
    public void setup() {
        cuisinesRegistry = new InMemoryCuisinesRegistry();
    }

    @Test
    public void shouldReturnRightCustomers() {
        register();

        assertThat(cuisinesRegistry.cuisineCustomers(FRENCH)).containsExactlyInAnyOrder(CUSTOMER1);
        assertThat(cuisinesRegistry.cuisineCustomers(GERMAN)).containsExactlyInAnyOrder(CUSTOMER1, CUSTOMER2, CUSTOMER3, CUSTOMER4);
        assertThat(cuisinesRegistry.cuisineCustomers(ITALIAN)).containsExactlyInAnyOrder(CUSTOMER3, CUSTOMER4);
        assertThat(cuisinesRegistry.cuisineCustomers(VIETNAMESE)).containsExactlyInAnyOrder(CUSTOMER3);
    }

    @Test
    public void shouldReturnRightCuisines() {
        register();

        assertThat(cuisinesRegistry.customerCuisines(CUSTOMER1)).containsExactlyInAnyOrder(FRENCH, GERMAN);
        assertThat(cuisinesRegistry.customerCuisines(CUSTOMER2)).containsExactlyInAnyOrder(GERMAN);
        assertThat(cuisinesRegistry.customerCuisines(CUSTOMER3)).containsExactlyInAnyOrder(ITALIAN, GERMAN, VIETNAMESE);
        assertThat(cuisinesRegistry.customerCuisines(CUSTOMER4)).containsExactlyInAnyOrder(GERMAN, ITALIAN);
    }

    @Test
    public void shouldHandleDuplicateRegisterCalls() {
        cuisinesRegistry.register(CUSTOMER1, FRENCH);
        cuisinesRegistry.register(CUSTOMER1, FRENCH);

        assertThat(cuisinesRegistry.customerCuisines(CUSTOMER1)).containsExactlyInAnyOrder(FRENCH);
    }

    @Test
    public void shouldReturnEmptyIfCustomerNotFound() {
        assertThat(cuisinesRegistry.customerCuisines(CUSTOMER1)).isEmpty();
    }

    @Test
    public void shouldReturnEmptyIfCuisineNotFound() {
        assertThat(cuisinesRegistry.cuisineCustomers(GERMAN)).isEmpty();
    }

    @Test
    public void shouldReturnEmptyIfNullCustomerProvided() {
        assertThat(cuisinesRegistry.customerCuisines(null)).isEmpty();
    }

    @Test
    public void shouldReturnEmptyIfNullCuisineProvided() {
        assertThat(cuisinesRegistry.cuisineCustomers(null)).isEmpty();
    }

    @Test
    public void shouldGetMostPopularCuisine() {
        register();

        assertThat(cuisinesRegistry.topCuisines(1)).containsExactly(GERMAN);
    }

    @Test
    public void shouldGetTwoMostPopularCuisines() {
        register();

        assertThat(cuisinesRegistry.topCuisines(2)).containsExactly(GERMAN, ITALIAN);
    }

    @Test
    public void shouldGetTwoMostPopularCuisinesEvenIfNIsBiggerThenTheNumberOfCuisines() {
        register();

        assertThat(cuisinesRegistry.topCuisines(40)).containsExactly(GERMAN, ITALIAN, VIETNAMESE, FRENCH);
    }

    @Test
    public void shouldReturnEmptyIfNonPositiveN() {
        assertThat(cuisinesRegistry.topCuisines(-1)).isEmpty();
    }

    @Test
    public void shouldReturnEmptyIfNoRegistrations() {
        assertThat(cuisinesRegistry.topCuisines(2)).isEmpty();
    }

    private void register() {
        cuisinesRegistry.register(CUSTOMER1, FRENCH);
        cuisinesRegistry.register(CUSTOMER1, GERMAN);
        cuisinesRegistry.register(CUSTOMER2, GERMAN);
        cuisinesRegistry.register(CUSTOMER3, ITALIAN);
        cuisinesRegistry.register(CUSTOMER3, VIETNAMESE);
        cuisinesRegistry.register(CUSTOMER3, GERMAN);
        cuisinesRegistry.register(CUSTOMER4, GERMAN);
        cuisinesRegistry.register(CUSTOMER4, ITALIAN);
    }
}