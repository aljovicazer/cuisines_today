# Cuisines Registry

## The story:

Cuisines Registry is an important part of Book-That-Table Inc. backend stack. It keeps in memory customer preferences for restaurant cuisines and is accessed by a bunch of components to register and retrieve data. 


The first iteration of this component was implemented by rather inexperienced developer and now may require some cleaning while new functionality is being added. But fortunately, according to his words: "Everything should work and please keep the test coverage as high as I did"


## Your tasks:
1. **[Important!]** Adhere to the boy scout rule. Leave your code better than you found it.
It is ok to change any code as long as the CuisinesRegistry interface remains unchanged.
2. Make is possible for customers to follow more than one cuisine (return multiple cuisines in de.quandoo.recruitment.registry.api.CuisinesRegistry#customerCuisines)
3. Implement de.quandoo.recruitment.registry.api.CuisinesRegistry#topCuisines - returning list of most popular (highest number of registered customers) ones
4. Create a short write up on how you would plan to scale this component to be able to process in-memory billions of customers and millions of cuisines (Book-That-Table is already planning for galactic scale). (100 words max)

## Submitting your solution

+ Fork it to a **[!]**private**[!]** gitlab repository (go to `Settings -> General -> Visibility, project features, permissions -> Project visibility`).
+ Put the write up mentioned in point 4. into the end of this file.
+ Share the project with gitlab user *quandoo_recruitment_task* (go to `Settings -> Members -> Invite member`, find the user in `Select members to invite` and set `Choose a role permission` to `Developer`)
+ Send us an **ssh** clone link to the repository.

## Design choises

+ I chose two maps which map from a customer or cuisine to a set of cuisines or customers.
+ In order to guarantee atomicity the register block is synchronized.
+ We will have fast writes, because inserting into a Map and then into a Set is done in constant time.
+ Reads will be slow, because data needs to be copied from a Set to a List (also to not share internal data) 
+ `topCuisines` is sorting a List, so it will be slow on a big data set.

## Scaling

+ If I understood this correctly, the scaled solution should be an in memory solution. 
Billions of customers are not going to fit into memory. That's hundreds of Gigabytes.
+ Redis could be used to solve this problem. Data could be spread on multiple shards, but it would still be in memory.
+ Two Redis tables could be used in the same way our HashMaps have been used as a key value pair. One table would have the customer as a key
and a `Set` of cuisines as a value. The other table is just the opposite. As long as our whole key (cuisine) fits into RAM, we won't have problems.
+ Very popular cuisines could potentially get too big for one shard. If that is the case we would need to partition the key in some way.
+ For the top cuisines we could have a new table that keeps a sorted List of lets say top 1000 cuisines. Every time when we 
are inserting a cuisine/customer pair we can check the size of the Redis Set and update the new table accordingly.